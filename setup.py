from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(
        name='housing-market',
        description='A model of housing, rent, and production',
        long_description=readme(),
        keywords='economics housing rent abm wealth',
        author='Kirsten Wright',
        author_email='k.w.robinson@gmail.cm',
)