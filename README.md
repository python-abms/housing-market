# Labour and housing market model

housing-market
A model of a simple housing market with a labour market with agglomeration modelled with a single simplified firm.

firm-entry
Model working towards multiple firms, and firm entry and exit. Workers choose to work or not. There is no demographic model, land market, or speculation.